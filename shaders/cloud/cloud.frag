#version 450
#extension GL_ARB_separate_shader_objects : enable

/////////////// STRUCTS ///////////////////////
struct Material
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 emissive;
    float specularExponent;
};
///////////////////////////////////////////////

///////////////// INPUTS //////////////////////
layout(location = 0) in vec3 inPos;
layout(location = 1) in vec4 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inUV;
layout(location = 4) in vec3 inLightPos;
layout(location = 5) in vec3 inCameraPos;
///////////////////////////////////////////////

/////////////////// UNIFORMS //////////////////
layout(set = 2, binding = 1) uniform ACTOR_MATERIAL_BLOCK
{
    Material material;
} actor_m;

layout(set = 2, binding = 2) uniform sampler2D textureSampler;
///////////////////////////////////////////////

////////////////////// OUTPUTS ////////////////
layout(location = 0) out vec4 outFBColor;
///////////////////////////////////////////////


void main() 
{
    vec3 fragPos = inPos.xyz;
    vec4 color = texture(textureSampler, inUV).rgba;
    outFBColor = color;
}