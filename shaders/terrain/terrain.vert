#version 450
#extension GL_ARB_separate_shader_objects : enable

/////////////// STRUCTS ///////////////////////
struct TRANSFORM
{
    vec4 position;
    vec4 rotation;
    vec4 scale;
    mat4 matrix;
};

struct CAMERA_PROJECTION
{
    mat4 matrix;
};
////////////////////////////////////////////////

/////////////////// VS INPUT ///////////////////
layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec4 inColor;
layout(location = 2) in vec4 inNormal;
layout(location = 3) in vec2 inUV;
////////////////////////////////////////////////

////////////////// UNIFORMS ////////////////////
layout(set = 0, binding = 0) uniform CAMERA_TRANSFORM_BLOCK
{
    TRANSFORM transform;
} camera;

layout(set = 0, binding = 1) uniform CAMERA_PROJECTION_BLOCK
{
    CAMERA_PROJECTION projection;
} camera_p;

layout(set = 1, binding = 0) uniform LIGHT_TRANSFORM_BLOCK
{
    TRANSFORM transform;
} light;

layout(set = 2, binding = 0) uniform ACTOR_BLOCK
{
    TRANSFORM transform;
} actor;
////////////////////////////////////////////////

/////////////////// PUSH CONSTANTS /////////////
layout(push_constant) uniform PushConstantShaderFeatures
{
    bool objectEnabled;
	bool texturingEnabled;
	bool lightingEnabled;
    int binStartX;
    int binStartY;
    int binSizeX;
    int binSizeY;
} pushConsts;
////////////////////////////////////////////////

///////////////// VS OUTPUTS /////////////////// 
layout(location = 0) out vec3 outFragPos;
layout(location = 1) out vec4 outFragColor;
layout(location = 2) out vec3 outFragNormal;
layout(location = 3) out vec2 outUV;
layout(location = 4) out vec3 outLightPos;
layout(location = 5) out vec3 outCameraPos;
layout(location = 6) out vec4 outColorHeight;
////////////////////////////////////////////////

vec4 colors[] = {
    vec4(0.85, 0.85, 0.85, 1.0f), // peaks - 0
    vec4(0.52, 0.47, 0.43, 1.0f), // rocks - 1
    vec4(0.419, 0.3, 0.094, 1.0f), // dirt - 2
    vec4(0.19, 0.27, 0.13, 1.0f), // dark green - 3
    vec4(0.20, 0.30, 0.16, 1.0f), // dark-li green - 4
    vec4(0.78, 0.72, 0.47, 1.0f), // sand - 5
    vec4(0.72, 0.83, 0.75, 1.0f) // underwater - 6
};

float maxHeights[] = {
    255,
    220,
    205,
    120,
    70,
    30,
    5
};

int intervals[] = {
    6, // underwater
    5, 5, 5, 5, 5, // sand
    4, 4, 4, 4, 4, 4, 4, 4, // light grass
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, // dark grass
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, // dirt
    1, 1, 1, // rocks
    0, 0, 0, 0, 0, 0, 0, 0 // peaks + 1 level
};

void main()
{
    gl_Position = camera_p.projection.matrix * camera.transform.matrix * actor.transform.matrix * inPosition;
    outFragPos = vec3(actor.transform.matrix * inPosition);
    outFragColor = inColor;
    outFragNormal = normalize((transpose(inverse(actor.transform.matrix)) * inNormal).xyz);
    outUV = inUV;
    outLightPos = normalize(light.transform.position.xyz - outFragPos);
    outCameraPos = normalize(camera.transform.position.xyz - outFragPos);

    int colorInterval = int(inPosition.y / 5.0f);
    int mainColorID = intervals[colorInterval];
    int nextColorID = intervals[colorInterval + 1];
    float maxHeight = maxHeights[mainColorID];
    float heightStep = 5;

    outColorHeight = mix(colors[nextColorID], colors[mainColorID], (maxHeight - inPosition.y) / heightStep);
}