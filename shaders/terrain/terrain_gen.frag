#version 450
#extension GL_ARB_separate_shader_objects : enable

/////////////// STRUCTS ///////////////////////
struct Material
{
    vec4 ambient;
    vec4 diffuse;
    vec4 specular;
    vec4 emissive;
    float specularExponent;
};
///////////////////////////////////////////////

///////////////// INPUTS //////////////////////
layout(location = 0) in vec3 inPos;
layout(location = 1) in vec4 inColor;
layout(location = 2) in vec3 inNormal;
layout(location = 3) in vec2 inUV;
layout(location = 4) in vec3 inLightPos;
layout(location = 5) in vec3 inCameraPos;
layout(location = 6) in vec4 inColorHeight;
layout(location = 7) in vec4 inDebugPos;
layout(location = 8) in vec4 debugNormal;

///////////////////////////////////////////////

/////////////////// UNIFORMS //////////////////
layout(set = 2, binding = 1) uniform ACTOR_MATERIAL_BLOCK
{
    Material material;
} actor_m;

layout(set = 2, binding = 2) uniform sampler2D textureSampler;
///////////////////////////////////////////////

/////////////// PUSH CONSTANTS ////////////////
layout(push_constant) uniform PushConstantShaderFeatures
{
    bool objectEnabled;
	bool texturingEnabled;
	bool lightingEnabled;
    int binStartX;
    int binStartY;
    int binSizeX;
    int binSizeY;
} pushConsts;
///////////////////////////////////////////////

////////////////////// OUTPUTS ////////////////
layout(location = 0) out vec4 outFBColor;
///////////////////////////////////////////////


void main() 
{
    if(!pushConsts.objectEnabled)
    {
        discard;
    }

    // Default white color
    vec3 color = inColorHeight.xyz;
    // Add texturing
    // if(pushConsts.texturingEnabled)
    // {
    //     color = texture(textureSampler, inUV).rgb;
    // }

    // Compute blinn-phong
    if(pushConsts.lightingEnabled)
    {
        vec3 ka = vec3(0.3f);
        vec3 ambient = ka * actor_m.material.ambient.xyz;

        vec3 fragPos = inPos.xyz;

        float kd = max(dot(inLightPos, inNormal), 0.0f);
        vec3 diffuse = kd * actor_m.material.diffuse.xyz;

        vec3 halfVec = normalize(inLightPos + inCameraPos);

        float ks = pow(max(dot(inNormal, halfVec), 0.0), actor_m.material.specularExponent);
        vec3 specular = ks * actor_m.material.specular.xyz;

        outFBColor = vec4(color * (ambient + diffuse + specular), 1.0f);
    }
    else
    {
        outFBColor = vec4(color, 1.0f);
    }
}