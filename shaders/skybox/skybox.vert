#version 450
#extension GL_ARB_separate_shader_objects : enable

/////////////// STRUCTS ///////////////////////
struct TRANSFORM
{
    vec4 position;
    vec4 rotation;
    vec4 scale;
    mat4 matrix;
};

struct CAMERA_PROJECTION
{
    mat4 matrix;
};
////////////////////////////////////////////////

/////////////////// VS INPUT ///////////////////
layout(location = 0) in vec4 inPosition;
layout(location = 1) in vec4 inColor;
layout(location = 2) in vec4 inNormal;
layout(location = 3) in vec2 inUV;
////////////////////////////////////////////////

////////////////// UNIFORMS ////////////////////
layout(set = 0, binding = 0) uniform CAMERA_TRANSFORM_BLOCK
{
    TRANSFORM transform;
} camera;

layout(set = 0, binding = 1) uniform CAMERA_PROJECTION_BLOCK
{
    CAMERA_PROJECTION projection;
} camera_p;

layout(set = 1, binding = 0) uniform ACTOR_BLOCK
{
    TRANSFORM transform;
} actor;
////////////////////////////////////////////////

///////////////// VS OUTPUTS /////////////////// 
layout(location = 0) out vec3 outUVW;
////////////////////////////////////////////////

void main()
{
    outUVW = inPosition.xyz;
    // Flip coordinates into vulkan space
    outUVW.xz *= -1.0f;
    vec4 position = camera_p.projection.matrix * camera.transform.matrix * actor.transform.matrix * inPosition;
    gl_Position = position.xyww;
}