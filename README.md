# High-Performance Graphics - Coursework 2
The official readme for CWK2.


## Cloning
This repository contains submodules for external dependencies. The first close must recursively add those:

```
git clone --recursive https://gitlab.com/sc17mae/hpg-cwk-2.git
```

Existing repositories can be updated manually:

```
git submodule init
git submodule update
```

## Build

Run the `build.bat` script.
The executable will be located in `build/src/Debug/FlightSimulator.exe`