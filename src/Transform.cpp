#include "Transform.h"
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/matrix_decompose.hpp>

/**
 * @brief Applies translation rotation scale
 * to generate the transform matrix
*/
void Transform::applyTRS()
{
	matrix = glm::mat4(1.0f);
	matrix = glm::translate(matrix, glm::vec3(position));
	matrix = glm::rotate(matrix, glm::radians(rotation.x), glm::vec3(1, 0, 0));
	matrix = glm::rotate(matrix, glm::radians(rotation.y), glm::vec3(0, 1, 0));
	matrix = glm::rotate(matrix, glm::radians(rotation.z), glm::vec3(0, 0, 1));
	matrix = glm::scale(matrix, glm::vec3(scale));
}

void Transform::decomposeUpdate()
{
	glm::vec3 scale;
	glm::quat rotation;
	glm::vec3 translation;
	glm::vec3 skew;
	glm::vec4 perspective;
	glm::decompose(matrix, scale, rotation, translation, skew, perspective);

	this->scale = glm::vec4(scale.x, scale.y, scale.z, 1.0f);
	glm::vec3 tempRot = glm::eulerAngles(rotation);
	this->rotation = glm::vec4(tempRot.x, tempRot.y, tempRot.z, 1.0f);
	this->position = glm::vec4(translation.x, translation.y, translation.z, 1.0f);
}
