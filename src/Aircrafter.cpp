#include "Aircrafter.h"
#include "VKC.h"
#include <glfw/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>

Aircrafter::Aircrafter()
{
	cameraSpring = 10.f;

	aircraft.transform.matrix = glm::lookAt(glm::vec3(aircraft.transform.position), glm::vec3(aircraft.transform.position) + glm::vec3(forward), glm::vec3(up));
	aircraft.transform.decomposeUpdate();
}

Aircrafter::~Aircrafter()
{
}

void Aircrafter::destroy()
{
	aircraft.destroy();
	//camera.destroy();
}

void Aircrafter::updateVectors()
{
}

void Aircrafter::processInput()
{
	if (glfwGetKey(VKC::get().window, GLFW_KEY_W) == GLFW_PRESS)
	{
		aircraft.transform.matrix = glm::rotate(aircraft.transform.matrix, glm::radians(1.f), glm::vec3(1, 0, 0));
	}
	if (glfwGetKey(VKC::get().window, GLFW_KEY_S) == GLFW_PRESS)
	{
		aircraft.transform.matrix = glm::rotate(aircraft.transform.matrix, glm::radians(-1.f), glm::vec3(1, 0, 0));
	}
	if (glfwGetKey(VKC::get().window, GLFW_KEY_A) == GLFW_PRESS)
	{
		aircraft.transform.matrix = glm::rotate(aircraft.transform.matrix, glm::radians(-1.f), glm::vec3(0, 1, 0));
	}
	if (glfwGetKey(VKC::get().window, GLFW_KEY_D) == GLFW_PRESS)
	{
		aircraft.transform.matrix = glm::rotate(aircraft.transform.matrix, glm::radians(1.f), glm::vec3(0, 1, 0));
	}
	if (glfwGetKey(VKC::get().window, GLFW_KEY_E) == GLFW_PRESS)
	{
		aircraft.transform.matrix = glm::rotate(aircraft.transform.matrix, glm::radians(1.f), glm::vec3(0, 0, 1));
	}
	if (glfwGetKey(VKC::get().window, GLFW_KEY_Q) == GLFW_PRESS)
	{
		aircraft.transform.matrix = glm::rotate(aircraft.transform.matrix, glm::radians(-1.f), glm::vec3(0, 0, 1));
	}
	if (glfwGetKey(VKC::get().window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		speed += 0.001f;
	}
	if (glfwGetKey(VKC::get().window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		speed -= 0.001f;
	}

	aircraft.transform.decomposeUpdate();
}

void Aircrafter::update(const float& dt)
{
//	aircraft.transform.position += glm::normalize(forward) * speed;
	auto fwd = glm::vec3(aircraft.transform.matrix[2][0], aircraft.transform.matrix[2][1], aircraft.transform.matrix[2][2]);

	aircraft.transform.matrix = glm::translate(aircraft.transform.matrix, glm::normalize(fwd) * speed);

	// TODO update camera position here
	// TODO update aircraft position here
}