#pragma once
#include <array>
#include <vulkan/vulkan.h>
#include "Transform.h"
#include <vector>

/**
 * @brief Camera class
 * This includes transform and projection
*/
class Camera
{
public:
	enum class MovementType
	{
		Forward,
		Backward,
		Left,
		Right,
		Up,
		Down,
		RollLeft,
		RollRight,
		PitchUp,
		PitchDown,
		YawLeft,
		YawRight
	};

public:
	// View
	Transform transform;
	glm::mat4 projection = glm::mat4(1.0f);

	glm::vec3 cfwd = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 cup = glm::vec3(0.0f, -1.0f, 0.0f);
	glm::vec3 cright = glm::vec3(1.0f, 0.0f, 0.0f);

	glm::vec3 forward = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 up = glm::vec3(0.0f, -1.0f, 0.0f);
	glm::vec3 right = glm::vec3(1.0f, 0.0f, 0.0f);

	float movementSpeed = 0.1f;

	// Vulkan specific
	VkDeviceSize projectionOffsetIntoBuffer = 0;
	VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
	std::vector<VkDescriptorSet> descriptorSets;

public:
	// Defaulted Constructor
	Camera() = default;

	void processKeyboard(const MovementType& type, float dt);

	void updateTransform();

	// Destroy method
	void destroy();

	// [Re] generation of descriptor sets
	void generateDSL(const bool& reset = false);

	bool isInside(const glm::vec4& point, const float& frustumSize = 1) const;
};

