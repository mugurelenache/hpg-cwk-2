#include "TextureCube.h"
#include "VKC.h"
#include <stb_image.h>
#include <iostream>
#include "Utility.h"

void TextureCube::load(const std::vector<std::string>& pathRLUDFB, const bool& persistentData)
{
	size_t layer = 0;
	for (const auto& path : pathRLUDFB)
	{
		data[layer] = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb_alpha);

		if (!data[layer])
		{
			std::cerr << "Error: Could not load texture." << std::endl;
			std::exit(-1);
		}

		layer++;
	}

	VkDeviceSize layerSize = static_cast<VkDeviceSize>(width * height * 4);
	VkDeviceSize size = static_cast<VkDeviceSize>(6 * layerSize);
	textureImageMipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(width, height)))) + 1;

	// Create a  staging buffer to transfer the data to the gpu
	Buffer stagingBuffer;
	stagingBuffer.create(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	stagingBuffer.bind();
	stagingBuffer.map(size);
	for (size_t i = 0; i < 6; i++)
	{
		stagingBuffer.populateWith(data[i], layerSize, layerSize * i);
	}
	stagingBuffer.unmap();

	if (!persistentData)
	{
		for (auto& d : data)
		{
			stbi_image_free(d);
		}
	}

	// Creating the image
	Utility::createImage(VKC::get().physicalDevice, VKC::get().device, textureImage, textureImageMemory, width, height, 1, textureImageMipLevels, 6, VK_SAMPLE_COUNT_1_BIT, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT);

	// Transitioning it to transfer dst
	Utility::transitionImageLayout(VKC::get().device, VKC::get().commandPool, VKC::get().graphicsQueue, textureImage, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, textureImageMipLevels, 6);

	for (size_t face = 0; face < 6; face++)
	{
		// Copy the data from buffer to image
		Utility::copyBufferToImage(VKC::get().device, VKC::get().commandPool, VKC::get().graphicsQueue, stagingBuffer.buffer, textureImage, width, height, face * layerSize, face);

		// Generates the mip maps for the texture
		Utility::generateMipMaps(VKC::get().physicalDevice, VKC::get().device, VKC::get().commandPool, VKC::get().graphicsQueue, textureImage, VK_FORMAT_R8G8B8A8_SRGB, static_cast<uint32_t>(width), static_cast<uint32_t>(height), textureImageMipLevels, face);
	}


	// Destroy the buffer as it's not needed
	stagingBuffer.destroy();

	// Creates the image view
	Utility::createImageView(VKC::get().device, textureImageView, textureImage, VK_IMAGE_VIEW_TYPE_CUBE, VK_FORMAT_R8G8B8A8_SRGB, VK_IMAGE_ASPECT_COLOR_BIT, textureImageMipLevels, 6);

	// Creates a sampler
	VkSamplerCreateInfo samplerCI = {};
	samplerCI.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerCI.minFilter = VK_FILTER_LINEAR;
	samplerCI.magFilter = VK_FILTER_LINEAR;
	samplerCI.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerCI.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerCI.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerCI.anisotropyEnable = VK_TRUE;

	// Checks the max anisotropy
	VkPhysicalDeviceProperties properties = {};
	vkGetPhysicalDeviceProperties(VKC::get().physicalDevice, &properties);

	samplerCI.maxAnisotropy = properties.limits.maxSamplerAnisotropy;
	samplerCI.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	samplerCI.unnormalizedCoordinates = VK_FALSE;
	samplerCI.compareEnable = VK_FALSE;
	samplerCI.compareOp = VK_COMPARE_OP_ALWAYS;
	// Mip mapping until maximum
	samplerCI.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerCI.mipLodBias = 0.0f;
	samplerCI.minLod = 0.0f;
	samplerCI.maxLod = static_cast<float>(textureImageMipLevels);

	// Try to create the sampler
	if (vkCreateSampler(VKC::get().device, &samplerCI, nullptr, &textureImageSampler) != VK_SUCCESS)
	{
		std::cerr << "Error: Could not create texture sampler." << std::endl;
		std::exit(-1);
	}
}

void TextureCube::destroy()
{
	vkDestroySampler(VKC::get().device, textureImageSampler, nullptr);
	vkDestroyImageView(VKC::get().device, textureImageView, nullptr);
	vkDestroyImage(VKC::get().device, textureImage, nullptr);
	vkFreeMemory(VKC::get().device, textureImageMemory, nullptr);
}
