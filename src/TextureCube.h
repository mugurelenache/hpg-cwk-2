#pragma once

#include <vulkan/vulkan.h>
#include <string>
#include <vector>

/**
 * @brief Texture Cube Class
 */
class TextureCube
{
public:
	int width = 0;
	int height = 0;
	int channels = 0;

	uint8_t* data[6] = {{}};

	//VkFormat format;
	VkImage textureImage = VK_NULL_HANDLE;
	VkImageView textureImageView = VK_NULL_HANDLE;
	VkSampler textureImageSampler = VK_NULL_HANDLE;
	VkDeviceMemory textureImageMemory = VK_NULL_HANDLE;
	uint32_t textureImageMipLevels = 0;

public:
	TextureCube() = default;

	void load(const std::vector<std::string>& pathRLUDFB, const bool& persistentData = false);
	void destroy();
};