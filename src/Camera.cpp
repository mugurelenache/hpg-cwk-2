#include "Camera.h"
#include <iostream>
#include "VKC.h"

#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/gtc/matrix_transform.hpp>

/**
 * @brief Destroys the descriptor set layout
*/
void Camera::processKeyboard(const MovementType& type, float dt)
{
	float velocity = movementSpeed * dt;

	switch (type)
	{
	case MovementType::Forward:
	{
		transform.position += velocity * glm::vec4(cfwd, 0.0f);
		break;
	}
	case MovementType::Backward:
	{
		transform.position -= velocity * glm::vec4(cfwd, 0.0f);
		break;
	}
	case MovementType::Left:
	{
		transform.position -= velocity * glm::vec4(cright, 0.0f);
		break;
	}
	case MovementType::Right:
	{
		transform.position += velocity * glm::vec4(cright, 0.0f);
		break;
	}
	case MovementType::Up:
	{
		transform.position -= velocity * glm::vec4(cup, 0.0f);
		break;
	}
	case MovementType::Down:
	{
		transform.position += velocity * glm::vec4(up, 0.0f);
		break;
	}
	case MovementType::PitchUp:
	{
		forward = glm::rotate(glm::mat4(1.0f), velocity, glm::vec3(right)) * glm::vec4(forward, 0.0f);
		up = glm::rotate(glm::mat4(1.0f), velocity, glm::vec3(right)) * glm::vec4(up, 0.0f);
		break;
	}
	case MovementType::PitchDown:
	{
		forward = glm::rotate(glm::mat4(1.0f), -velocity, glm::vec3(right)) * glm::vec4(forward, 0.0f);
		up = glm::rotate(glm::mat4(1.0f), -velocity, glm::vec3(right)) * glm::vec4(up, 0.0f);

		break;
	}
	case MovementType::RollLeft:
	{
		up = glm::rotate(glm::mat4(1.0f), -velocity, glm::vec3(forward)) * glm::vec4(up, 0.0f);
		right = glm::rotate(glm::mat4(1.0f), -velocity, glm::vec3(forward)) * glm::vec4(right, 0.0f);
		break;
	}
	case MovementType::RollRight:
	{
		up = glm::rotate(glm::mat4(1.0f), velocity, glm::vec3(forward)) * glm::vec4(up, 0.0f);
		right = glm::rotate(glm::mat4(1.0f), velocity, glm::vec3(forward)) * glm::vec4(right, 0.0f);
		break;
	}
	case MovementType::YawLeft:
	{
		forward = glm::rotate(glm::mat4(1.0f), velocity, glm::vec3(up)) * glm::vec4(forward, 0.0f);
		right = glm::rotate(glm::mat4(1.0f), velocity, glm::vec3(up)) * glm::vec4(right, 0.0f);
		break;
	}
	case MovementType::YawRight:
	{
		forward = glm::rotate(glm::mat4(1.0f), -velocity, glm::vec3(up)) * glm::vec4(forward, 0.0f);
		right = glm::rotate(glm::mat4(1.0f), -velocity, glm::vec3(up)) * glm::vec4(right, 0.0f);
		break;
	}
	}
}

void Camera::updateTransform()
{
	transform.matrix = glm::lookAt(glm::vec3(transform.position), glm::vec3(transform.position) + glm::vec3(forward), glm::vec3(up));
}

/**
 * @brief Destroys the things attached to the camera
*/
void Camera::destroy()
{
	vkDestroyDescriptorSetLayout(VKC::get().device, descriptorSetLayout, nullptr);
}

/**
 * @brief [RE]generate the descriptor sets and layouts
 * @param reset
*/
void Camera::generateDSL(const bool& reset)
{
	if (reset)
	{
		vkDestroyDescriptorSetLayout(VKC::get().device, descriptorSetLayout, nullptr);
	}

	// Creating the bindings for the vertex stages
	// Mainly we have the camera transform and the camera projection
	// uniforms that will be mapped in the shader
	std::vector<VkDescriptorSetLayoutBinding> bindings;
	{
		VkDescriptorSetLayoutBinding binding = {};
		binding.binding = 0;
		binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		binding.descriptorCount = 1;
		binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		binding.pImmutableSamplers = nullptr;
		bindings.push_back(binding);
	}
	{
		VkDescriptorSetLayoutBinding binding = {};
		binding.binding = 1;
		binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		binding.descriptorCount = 1;
		binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		binding.pImmutableSamplers = nullptr;
		bindings.push_back(binding);
	}

	// Create the descriptor set layout CI
	VkDescriptorSetLayoutCreateInfo dslCI = {};
	dslCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	dslCI.bindingCount = static_cast<uint32_t>(bindings.size());
	dslCI.pBindings = bindings.data();

	// Try to create the descriptor set layout
	if (vkCreateDescriptorSetLayout(VKC::get().device, &dslCI, nullptr, &descriptorSetLayout) != VK_SUCCESS)
	{
		std::cerr << "Error: cannot create descriptor set layout" << std::endl;
		std::exit(-1);
	}

	// Resize the layouts
	std::vector<VkDescriptorSetLayout> layouts(VKC::get().swapchainImages.size(), descriptorSetLayout);

	// Fill in the allocate info for the descriptor sets
	VkDescriptorSetAllocateInfo dsAI = {};
	dsAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	dsAI.descriptorPool = VKC::get().descriptorPool;
	dsAI.descriptorSetCount = static_cast<uint32_t>(VKC::get().swapchainImages.size());
	dsAI.pSetLayouts = layouts.data();

	descriptorSets.resize(VKC::get().swapchainImages.size());

	//  Try to allocate the descriptor sets
	if (vkAllocateDescriptorSets(VKC::get().device, &dsAI, descriptorSets.data()) != VK_SUCCESS)
	{
		std::cerr << "Error: failed to allocate descriptor sets." << std::endl;
		std::exit(-1);
	}

	// Update the descriptor sets via the descriptor writes
	for (size_t i = 0; i < descriptorSets.size(); i++)
	{
		std::vector<VkWriteDescriptorSet> descriptorSetWrites;
		//  TRANSFORM - VIEW
		VkDescriptorBufferInfo descriptorBI = {};
		descriptorBI.buffer = VKC::get().uniformBuffers[i].buffer;
		descriptorBI.offset = transform.offsetIntoBuffer;
		descriptorBI.range = sizeof(Transform);

		{
			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = descriptorSets[i];
			descriptorWrite.dstBinding = 0;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = &descriptorBI;
			descriptorWrite.pImageInfo = nullptr;
			descriptorWrite.pTexelBufferView = nullptr;
			descriptorSetWrites.push_back(descriptorWrite);
		}

		// Projection
		VkDescriptorBufferInfo descriptorPI = {};
		descriptorPI.buffer = VKC::get().uniformBuffers[i].buffer;
		descriptorPI.offset = projectionOffsetIntoBuffer;
		descriptorPI.range = sizeof(glm::mat4);

		{
			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = descriptorSets[i];
			descriptorWrite.dstBinding = 1;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = &descriptorPI;
			descriptorWrite.pImageInfo = nullptr;
			descriptorWrite.pTexelBufferView = nullptr;
			descriptorSetWrites.push_back(descriptorWrite);
		}

		vkUpdateDescriptorSets(VKC::get().device, static_cast<uint32_t>(descriptorSetWrites.size()), descriptorSetWrites.data(), 0, nullptr);
	}
}

bool Camera::isInside(const glm::vec4& point, const float& frustumSize) const
{
	auto projected = projection * transform.matrix * point;
	projected /= projected.w;

	if (projected.x < frustumSize && projected.x > -frustumSize &&
		projected.y < frustumSize && projected.y > -frustumSize &&
		projected.z < frustumSize && projected.z > -frustumSize) 
	{
		return true;
	}
	
	return false;
}
