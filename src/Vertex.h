#pragma once
#include <glm/glm.hpp>
#include <vector>
#include <vulkan/vulkan.h>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>

/**
 * @brief Vertex class
 * Contains position, color, normal, tex coords
*/
class Vertex
{
public:
	glm::vec4 pos;
	glm::vec4 color;
	glm::vec4 normal;
	glm::vec2 texCoord;

public:
	// Default constructor
	Vertex();
	// Constructor with params
	Vertex(const glm::vec4& pos, const glm::vec4& color, const glm::vec4& normal, const glm::vec2& texCoord);

	// Generates binding descriptions
	static VkVertexInputBindingDescription getBindingDescription();

	// Generates the attribute descriptions
	static std::vector<VkVertexInputAttributeDescription> getAttributeDescriptions();

	// Overloaded equals operator
	bool operator==(const Vertex& other) const;
};

// Custom hashing
namespace std
{
	template<> struct hash<Vertex>
	{
		size_t operator()(Vertex const& vertex) const
		{
			return ((hash<glm::vec4>()(vertex.pos) ^ (hash<glm::vec4>()(vertex.color) << 1)) >> 1) ^ (hash<glm::vec4>()(vertex.color) ^ (hash<glm::vec2>()(vertex.texCoord) << 1));
		}
	};
}
