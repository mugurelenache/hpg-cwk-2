#pragma once
#include "Mesh.h"
#include "Transform.h"
#include "Texture.h"

/**
 * @brief Terrain class
*/
class Terrain
{
public:
	struct Bin
	{
		// 8 Vertices define a bin
		std::vector<glm::vec4> vertices;

		Bin(const std::vector<glm::vec4>& vertices) : vertices(vertices) {}
	};
	
	Mesh mesh;
	Transform transform;
	Texture heightfield;

	VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
	std::vector<VkDescriptorSet> descriptorSets;

	VkDescriptorSetLayout heightmapDescriptorSetLayout = VK_NULL_HANDLE;
	std::vector<VkDescriptorSet> heightmapDescriptorSets;
	
	std::vector<Bin> bins;
	std::vector<bool> visibleBins;

	int32_t binsX = 1;
	int32_t binsY = 1;

public:
	Terrain() = default;

	// Generates a terrain via heightfield
	void generateViaHeightfield(const std::string& path);

	void destroy();

	// For part 3
	// Method that generates the descriptor set layout
	void generateDSL(const bool& reset = false);

	void generateBins();
};

