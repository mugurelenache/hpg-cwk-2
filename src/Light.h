#pragma once
#include <vulkan/vulkan.h>
#include "Transform.h"
#include <vector>

/**
 * @brief Light class
 * Including transform only
*/
class Light
{
public:
	Transform transform;

	// Vulkan Specific
	VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
	std::vector<VkDescriptorSet> descriptorSets;

public:
	// Defaulted light constructor
	Light() = default;

	// Destroy method
	void destroy();

	// Generate descriptor set layout method
	void generateDSL(const bool& reset = false);
};

