#pragma once
#include <glm/glm.hpp>
#include <vulkan/vulkan.h>

/**
 * @brief Material class
*/
class Material
{
public:
	glm::vec4 ambient = glm::vec4(0.1f);
	glm::vec4 diffuse = glm::vec4(0.2f);
	glm::vec4 specular = glm::vec4(0.3f);
	glm::vec4 emissive = glm::vec4(0.0f);
	alignas(4) float specularExponent = 4.0f;

	// Vulkan specific
	VkDeviceSize offsetIntoBuffer = 0;

public:
	// Defaulted constructor
	Material() = default;
};

