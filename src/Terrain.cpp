#include "Terrain.h"
#include <random>
#include <iostream>
#include <chrono>
#include "VKC.h"

void Terrain::generateViaHeightfield(const std::string& path)
{
	heightfield.load(path, true);

	std::vector<Vertex> vertexData;
	std::vector<uint32_t> indexData;

	float offsetX = static_cast<float>(heightfield.width) / 2.f;
	float offsetY = static_cast<float>(heightfield.height) / 2.f;
	
	for (uint32_t i = 0; i < heightfield.height; i++)
	{
		for (uint32_t j = 0; j < heightfield.width; j++)
		{
			// Position calculation
			Vertex vertex;
			auto pix = heightfield.pixelToGrayscale(heightfield.getPixel(j, i));
			vertex.pos = glm::vec4(j - offsetX, pix, i - offsetY, 1.0f);
			vertex.texCoord = glm::vec2(static_cast<float>(j) / static_cast<float>(heightfield.width), static_cast<float>(i) / static_cast<float>(heightfield.height));

			// Normal calculation
			if (i == 0 || j == 0 || i == heightfield.height - 1 || j == heightfield.width - 1)
			{
				vertex.normal = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);
			}
			else
			{
				float pixL = heightfield.pixelToGrayscale(heightfield.getPixel(j - 1, i));
				float pixR = heightfield.pixelToGrayscale(heightfield.getPixel(j + 1, i));
				float pixU = heightfield.pixelToGrayscale(heightfield.getPixel(j, i - 1));
				float pixD = heightfield.pixelToGrayscale(heightfield.getPixel(j, i + 1));

				vertex.normal = glm::vec4((pixL - pixR) / 2.0f, 1.0f, (pixU - pixD) / 2.0f, 1.0f);
			}

			vertexData.push_back(vertex);

			// Create the index patch
			if (!(i == heightfield.height - 1 || j == heightfield.width - 1))
			{
				indexData.push_back(i * heightfield.width + j);
				indexData.push_back(i * heightfield.width + (j + 1));
				indexData.push_back((i + 1) * heightfield.width + j);

				indexData.push_back(i * heightfield.width + (j + 1));
				indexData.push_back((i + 1) * heightfield.width + j + 1);
				indexData.push_back((i + 1) * heightfield.width + j);
			}
		}
	}

	mesh.vertices = vertexData;
	mesh.indices = indexData;
}

void Terrain::destroy()
{
	mesh.destroy();
	heightfield.destroy();
	vkDestroyDescriptorSetLayout(VKC::get().device, descriptorSetLayout, nullptr);
	vkDestroyDescriptorSetLayout(VKC::get().device, heightmapDescriptorSetLayout, nullptr);
}

void Terrain::generateDSL(const bool& reset)
{
	if (reset)
	{
		vkDestroyDescriptorSetLayout(VKC::get().device, descriptorSetLayout, nullptr);
		vkDestroyDescriptorSetLayout(VKC::get().device, heightmapDescriptorSetLayout, nullptr);
	}

	// Descriptor set layout
	{
		// Create the bindings
		std::vector<VkDescriptorSetLayoutBinding> bindings;
		{
			VkDescriptorSetLayoutBinding binding = {};
			binding.binding = 0;
			binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			binding.descriptorCount = 1;
			binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
			binding.pImmutableSamplers = nullptr;
			bindings.push_back(binding);
		}

		{
			VkDescriptorSetLayoutBinding binding = {};
			binding.binding = 1;
			binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			binding.descriptorCount = 1;
			binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
			binding.pImmutableSamplers = nullptr;
			bindings.push_back(binding);
		}

		{
			VkDescriptorSetLayoutBinding binding = {};
			binding.binding = 2;
			binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			binding.descriptorCount = 1;
			binding.stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
			binding.pImmutableSamplers = nullptr;
			bindings.push_back(binding);
		}

		VkDescriptorSetLayoutCreateInfo dslCI = {};
		dslCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		dslCI.bindingCount = static_cast<uint32_t>(bindings.size());
		dslCI.pBindings = bindings.data();

		// Create the descriptor set layout
		if (vkCreateDescriptorSetLayout(VKC::get().device, &dslCI, nullptr, &descriptorSetLayout) != VK_SUCCESS)
		{
			std::cerr << "Error: cannot create descriptor set layout" << std::endl;
			std::exit(-1);
		}

		std::vector<VkDescriptorSetLayout> layouts(VKC::get().swapchainImages.size(), descriptorSetLayout);

		VkDescriptorSetAllocateInfo dsAI = {};
		dsAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
		dsAI.descriptorPool = VKC::get().descriptorPool;
		dsAI.descriptorSetCount = static_cast<uint32_t>(VKC::get().swapchainImages.size());
		dsAI.pSetLayouts = layouts.data();

		descriptorSets.resize(VKC::get().swapchainImages.size());

		// Allocating the descriptor sets
		if (vkAllocateDescriptorSets(VKC::get().device, &dsAI, descriptorSets.data()) != VK_SUCCESS)
		{
			std::cerr << "Error: failed to allocate descriptor sets." << std::endl;
			std::exit(-1);
		}

		// Creating the writes and updating the descriptor sets
		for (size_t i = 0; i < descriptorSets.size(); i++)
		{
			std::vector<VkWriteDescriptorSet> descriptorSetWrites;

			//  TRANSFORM
			VkDescriptorBufferInfo descriptorBI = {};
			descriptorBI.buffer = VKC::get().uniformBuffers[i].buffer;
			descriptorBI.offset = transform.offsetIntoBuffer;
			descriptorBI.range = sizeof(Transform);

			{
				VkWriteDescriptorSet descriptorWrite = {};
				descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				descriptorWrite.dstSet = descriptorSets[i];
				descriptorWrite.dstBinding = 0;
				descriptorWrite.dstArrayElement = 0;
				descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
				descriptorWrite.descriptorCount = 1;
				descriptorWrite.pBufferInfo = &descriptorBI;
				descriptorWrite.pImageInfo = nullptr;
				descriptorWrite.pTexelBufferView = nullptr;
				descriptorSetWrites.push_back(descriptorWrite);
			}

			// Material
			VkDescriptorBufferInfo descriptorMI = {};
			descriptorMI.buffer = VKC::get().uniformBuffers[i].buffer;
			descriptorMI.offset = mesh.material.offsetIntoBuffer;
			descriptorMI.range = sizeof(Material);

			{
				VkWriteDescriptorSet descriptorWrite = {};
				descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				descriptorWrite.dstSet = descriptorSets[i];
				descriptorWrite.dstBinding = 1;
				descriptorWrite.dstArrayElement = 0;
				descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
				descriptorWrite.descriptorCount = 1;
				descriptorWrite.pBufferInfo = &descriptorMI;
				descriptorWrite.pImageInfo = nullptr;
				descriptorWrite.pTexelBufferView = nullptr;
				descriptorSetWrites.push_back(descriptorWrite);
			}

			// IMAGE DSW
			VkDescriptorImageInfo descriptorII = {};
			descriptorII.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
			descriptorII.imageView = heightfield.textureImageView;
			descriptorII.sampler = heightfield.textureImageSampler;

			{
				VkWriteDescriptorSet descriptorWrite = {};
				descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
				descriptorWrite.dstSet = descriptorSets[i];
				descriptorWrite.dstBinding = 2;
				descriptorWrite.dstArrayElement = 0;
				descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
				descriptorWrite.descriptorCount = 1;
				descriptorWrite.pBufferInfo = nullptr;
				descriptorWrite.pImageInfo = &descriptorII;
				descriptorWrite.pTexelBufferView = nullptr;
				descriptorSetWrites.push_back(descriptorWrite);
			}

			vkUpdateDescriptorSets(VKC::get().device, static_cast<uint32_t>(descriptorSetWrites.size()), descriptorSetWrites.data(), 0, nullptr);
		}
	}

	// Heightmap Descriptor set layout
	{
	// Create the bindings
	std::vector<VkDescriptorSetLayoutBinding> bindings;
	{
		VkDescriptorSetLayoutBinding binding = {};
		binding.binding = 0;
		binding.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
		binding.descriptorCount = 1;
		binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		binding.pImmutableSamplers = nullptr;
		bindings.push_back(binding);
	}

	VkDescriptorSetLayoutCreateInfo dslCI = {};
	dslCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	dslCI.bindingCount = static_cast<uint32_t>(bindings.size());
	dslCI.pBindings = bindings.data();

	// Create the descriptor set layout
	if (vkCreateDescriptorSetLayout(VKC::get().device, &dslCI, nullptr, &heightmapDescriptorSetLayout) != VK_SUCCESS)
	{
		std::cerr << "Error: cannot create descriptor set layout" << std::endl;
		std::exit(-1);
	}

	std::vector<VkDescriptorSetLayout> layouts(VKC::get().swapchainImages.size(), heightmapDescriptorSetLayout);

	VkDescriptorSetAllocateInfo dsAI = {};
	dsAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	dsAI.descriptorPool = VKC::get().descriptorPool;
	dsAI.descriptorSetCount = static_cast<uint32_t>(VKC::get().swapchainImages.size());
	dsAI.pSetLayouts = layouts.data();

	heightmapDescriptorSets.resize(VKC::get().swapchainImages.size());

	// Allocating the descriptor sets
	if (vkAllocateDescriptorSets(VKC::get().device, &dsAI, heightmapDescriptorSets.data()) != VK_SUCCESS)
	{
		std::cerr << "Error: failed to allocate descriptor sets." << std::endl;
		std::exit(-1);
	}

	// Creating the writes and updating the descriptor sets
	for (size_t i = 0; i < heightmapDescriptorSets.size(); i++)
	{
		std::vector<VkWriteDescriptorSet> descriptorSetWrites;

		// IMAGE DSW
		VkDescriptorImageInfo descriptorII = {};
		descriptorII.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		descriptorII.imageView = heightfield.textureImageView;
		descriptorII.sampler = heightfield.textureImageSampler;

		{
			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = heightmapDescriptorSets[i];
			descriptorWrite.dstBinding = 0;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = nullptr;
			descriptorWrite.pImageInfo = &descriptorII;
			descriptorWrite.pTexelBufferView = nullptr;
			descriptorSetWrites.push_back(descriptorWrite);
		}

		vkUpdateDescriptorSets(VKC::get().device, static_cast<uint32_t>(descriptorSetWrites.size()), descriptorSetWrites.data(), 0, nullptr);
	}
	}

}

void Terrain::generateBins()
{
	bins.clear();
	visibleBins.resize(binsX * binsY, false);

	float offsetX = heightfield.width / static_cast<float>(binsX);
	float offsetY = heightfield.height / static_cast<float>(binsY);

	float halfX = heightfield.width / 2.f;
	float halfY = heightfield.height / 2.f;
	
	
	for(int32_t i = 0; i < binsY; i++)
	{
		for(int32_t j = 0; j < binsX; j++)
		{
			std::vector<glm::vec4> corners = {
				glm::vec4(j * offsetX - halfX, 0, i * offsetY - halfY, 1.0f),
				glm::vec4(j * offsetX - halfX , 0, (i + 1) * offsetY - halfY, 1.0f),
				glm::vec4((j + 1) * offsetX - halfX, 0, (i + 1) * offsetY - halfY, 1.0f),
				glm::vec4((j + 1) * offsetX - halfX, 0, i * offsetY - halfY, 1.0f),
				glm::vec4(j * offsetX - halfX, 255, i * offsetY - halfY, 1.0f),
				glm::vec4(j * offsetX - halfX, 255, (i+1)* offsetY - halfY, 1.0f),
				glm::vec4((j + 1) * offsetX - halfX, 255, (i + 1) * offsetY - halfY, 1.0f),
				glm::vec4((j + 1)* offsetX - halfX, 255, i * offsetY - halfY, 1.0f),
			};
			
			bins.emplace_back(Bin(corners));
			visibleBins[i * binsX + j] = true;
		}
	}
}
