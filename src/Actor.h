#pragma once
#include "Mesh.h"
#include "Transform.h"
#include "Material.h"
#include "Texture.h"

/**
 * @brief Actor class
 *
 * Although it's inheritance over composition :(
*/
class Actor
{
public:
	Mesh mesh;
	Transform transform;
	Texture texture;

	VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
	std::vector<VkDescriptorSet> descriptorSets;

public:
	// Defaulted constructor
	Actor() = default;

	// Destroy method for the resources
	void destroy();

	// [Re]Generate descriptor set layouts
	void generateDSL(const bool& reset = false);
};

