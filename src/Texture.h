#pragma once
#include <vulkan/vulkan.h>
#include <string>
#include <vector>

/**
 * @brief Texture class
 * Includes memory associated, sampler, image & image views
*/
class Texture
{
public:
	int width = 0;
	int height = 0;
	int channels = 0;

	unsigned char* pixels;

	VkImage textureImage = VK_NULL_HANDLE;
	VkImageView textureImageView = VK_NULL_HANDLE;
	VkSampler textureImageSampler = VK_NULL_HANDLE;
	VkDeviceMemory textureImageMemory = VK_NULL_HANDLE;
	uint32_t textureImageMipLevels = 0;
	VkFormat format = VK_FORMAT_R8G8B8A8_UNORM;

public:
	// Defaulted constructor
	Texture() = default;

	// Method to load a texture
	void load(const std::string& path, const bool& persistentData = false, const VkFormat& format = VK_FORMAT_R8G8B8A8_UNORM);

	std::vector<unsigned char> getPixel(const uint32_t& x, const uint32_t& y);

	unsigned char pixelToGrayscale(const std::vector<unsigned char>& pixel);

	// Method to destroy the resources
	void destroy();
};