#pragma once
#include <vulkan/vulkan.h>
#include <vector>

/**
 * @brief Generic buffer data structure
*/
class Buffer
{
public:
	// Vulkan specific
	VkBuffer buffer = VK_NULL_HANDLE;
	VkDeviceMemory memory = VK_NULL_HANDLE;
	VkDeviceSize size = 0;

	// Ptr to CPU accessible
	void* mapping = nullptr;
	std::vector<VkDeviceSize> offsets = { 0 };

public:
	// Defaulted constructor
	Buffer() = default;

	// Module setup
	void create(const VkDeviceSize& size, const VkBufferUsageFlags& usage, const VkMemoryPropertyFlags& memoryProperties);

	// Destroy method
	void destroy();

	// Mapping & Unmapping methods
	VkResult map(const VkDeviceSize& size, const VkDeviceSize& offset = 0);
	void unmap();

	// Binding
	VkResult bind(const VkDeviceSize& offset = 0);

	// Copy & populating
	void populateWith(void* data, const VkDeviceSize& size, const VkDeviceSize& offset = 0);
	void copyInto(Buffer& other, const VkDeviceSize& size, const VkDeviceSize& srcOffset = 0, const VkDeviceSize& dstOffset = 0);

	// Operations
	VkResult invalidate(const VkDeviceSize& size, const VkDeviceSize& offset = 0);
	VkResult flush(const VkDeviceSize& size, const VkDeviceSize& offset = 0);

	// Adding element to buffer (mainly used in uniform buffer)
	VkDeviceSize addElementToBuffer(const VkDeviceSize& size);
};

