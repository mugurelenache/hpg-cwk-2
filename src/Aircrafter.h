#pragma once
#include "Camera.h"
#include "Actor.h"


/**
 * @brief Aircrafter class with camera attached
*/
class Aircrafter
{
public:
	Actor aircraft;
	Camera camera;
	float cameraSpring;
	float speed = 0.0f;

	glm::vec4 up = glm::vec4(0.0f, -1.0f, 0.0f, 0.0f);
	glm::vec4 forward = glm::vec4(0.0f, 0.0f, 1.0f, 0.0f);

public:
	Aircrafter();
	~Aircrafter();

	void destroy();

	void updateVectors();
	void processInput();
	void update(const float& dt);
};

