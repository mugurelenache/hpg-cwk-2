#include "Buffer.h"
#include <iostream>
#include "VKC.h"
#include "Utility.h"
#include <cstring>

/**
 * @brief Creates a general buffer
 * @param size
 * @param usage
 * @param memoryProperties
*/
void Buffer::create(const VkDeviceSize& size, const VkBufferUsageFlags& usage, const VkMemoryPropertyFlags& memoryProperties)
{
	this->size = size;

	// Filling in the buffer CI
	VkBufferCreateInfo bufferCI = {};
	bufferCI.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	bufferCI.size = size;
	bufferCI.usage = usage;
	bufferCI.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

	// Tries to create a buffer
	if (vkCreateBuffer(VKC::get().device, &bufferCI, nullptr, &buffer) != VK_SUCCESS)
	{
		std::cerr << "Error: cannot create buffer." << std::endl;
		std::exit(-1);
	}

	// Gets the memory requirements
	VkMemoryRequirements memoryReqs;
	vkGetBufferMemoryRequirements(VKC::get().device, buffer, &memoryReqs);

	// Filling in the allocation info
	// The memory type index is found dynamically given the type bits in the requirements
	// as well as the memory properties
	VkMemoryAllocateInfo memAllocI = {};
	memAllocI.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	memAllocI.allocationSize = memoryReqs.size;
	memAllocI.memoryTypeIndex = Utility::findMemoryType(VKC::get().physicalDevice, memoryReqs.memoryTypeBits, memoryProperties);

	// Tries to allocate a buffer
	if (vkAllocateMemory(VKC::get().device, &memAllocI, nullptr, &memory) != VK_SUCCESS)
	{
		std::cerr << "Error: Could not allocate memory." << std::endl;
		std::exit(-1);
	}
}


/**
 * @brief Destorys a buffer and frees the memory associated
*/
void Buffer::destroy()
{
	if (buffer)
	{
		vkDestroyBuffer(VKC::get().device, buffer, nullptr);
	}

	if (memory)
	{
		vkFreeMemory(VKC::get().device, memory, nullptr);
	}
}

/**
 * @brief Maps the buffer to the mapping member
 * @param size
 * @param offset
 * @return
*/
VkResult Buffer::map(const VkDeviceSize& size, const VkDeviceSize& offset)
{
	return vkMapMemory(VKC::get().device, memory, offset, size, 0, &mapping);
}


/**
 * @brief Unmaps the buffer from the memory
*/
void Buffer::unmap()
{
	if (mapping)
	{
		vkUnmapMemory(VKC::get().device, memory);
		mapping = nullptr;
	}
}


/**
 * @brief Binds a buffer memory
 * @param offset
 * @return
*/
VkResult Buffer::bind(const VkDeviceSize& offset)
{
	return vkBindBufferMemory(VKC::get().device, buffer, memory, offset);
}


/**
 * @brief Populates a buffer with data
 * @param data
 * @param size
 * @param offset
*/
void Buffer::populateWith(void* data, const VkDeviceSize& size, const VkDeviceSize& offset)
{
	if (mapping)
	{
		char* ptr = reinterpret_cast<char*>(mapping);
		std::memcpy(ptr + offset, data, static_cast<size_t>(size));
	}
	else
	{
		std::cerr << "Error: cannot copy data into buffer." << std::endl;
		std::exit(-1);
	}
}


/**
 * @brief Copies a buffer into another buffer
 * @param other
 * @param size
 * @param srcOffset
 * @param dstOffset
*/
void Buffer::copyInto(Buffer& other, const VkDeviceSize& size, const VkDeviceSize& srcOffset, const VkDeviceSize& dstOffset)
{
	// Create a cmd buffer AI
	VkCommandBufferAllocateInfo cmdBufferAI = {};
	cmdBufferAI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	cmdBufferAI.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	cmdBufferAI.commandPool = VKC::get().commandPool;
	cmdBufferAI.commandBufferCount = 1;

	// Allocate a cmd buffer
	VkCommandBuffer cmdBuffer;
	vkAllocateCommandBuffers(VKC::get().device, &cmdBufferAI, &cmdBuffer);

	// Begin the cmd buffer
	VkCommandBufferBeginInfo cmdBufferBI = {};
	cmdBufferBI.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	cmdBufferBI.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	vkBeginCommandBuffer(cmdBuffer, &cmdBufferBI);
	{
		// Copy the region into the other buffer
		VkBufferCopy bufferCopy = {};
		bufferCopy.srcOffset = srcOffset;
		bufferCopy.dstOffset = dstOffset;
		bufferCopy.size = size;

		vkCmdCopyBuffer(cmdBuffer, buffer, other.buffer, 1, &bufferCopy);
	}
	vkEndCommandBuffer(cmdBuffer);

	// Submit it to the queue and wait for the copy fence
	VkSubmitInfo submitI = {};
	submitI.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	submitI.commandBufferCount = 1;
	submitI.pCommandBuffers = &cmdBuffer;

	VkFence copyFence = VK_NULL_HANDLE;
	VkFenceCreateInfo fenceCI = {};
	fenceCI.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

	// Try to create a fence
	// so that we know when the copy has finished
	if (vkCreateFence(VKC::get().device, &fenceCI, nullptr, &copyFence) != VK_SUCCESS)
	{
		std::cerr << "Error: Cannot create copy fence." << std::endl;
		std::exit(-1);
	}

	vkQueueSubmit(VKC::get().transferQueue, 1, &submitI, copyFence);
	vkWaitForFences(VKC::get().device, 1, &copyFence, VK_TRUE, UINT64_MAX);

	// Free the cmd buffer and destroy the fence
	vkFreeCommandBuffers(VKC::get().device, VKC::get().commandPool, 1, &cmdBuffer);
	vkDestroyFence(VKC::get().device, copyFence, nullptr);
}


/**
 * @brief Invalidates a buffer memory range
 * @param size
 * @param offset
 * @return
*/
VkResult Buffer::invalidate(const VkDeviceSize& size, const VkDeviceSize& offset)
{
	VkMappedMemoryRange mappedRange = {};
	mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
	mappedRange.memory = memory;
	mappedRange.offset = offset;
	mappedRange.size = size;
	return vkInvalidateMappedMemoryRanges(VKC::get().device, 1, &mappedRange);
}


/**
 * @brief Flushes a memory range
 * @param size
 * @param offset
 * @return
*/
VkResult Buffer::flush(const VkDeviceSize& size, const VkDeviceSize& offset)
{
	VkMappedMemoryRange mappedRange = {};
	mappedRange.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
	mappedRange.memory = memory;
	mappedRange.offset = offset;
	mappedRange.size = size;
	return vkFlushMappedMemoryRanges(VKC::get().device, 1, &mappedRange);
}


/**
 * @brief Adds an element to the buffer and calculates its offset.
 * @param size
 * @return
*/
VkDeviceSize Buffer::addElementToBuffer(const VkDeviceSize& size)
{
	offsets.push_back(Utility::calculateAllocSize(size) + offsets[offsets.size() - 1]);
	return offsets[offsets.size() - 2];
}
