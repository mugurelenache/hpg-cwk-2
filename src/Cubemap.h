#pragma once

#include "Mesh.h"
#include "Transform.h"
#include "Material.h"
#include "TextureCube.h"

class Cubemap
{
public:
	Mesh mesh;
	Transform transform;
	TextureCube texture;

	VkDescriptorSetLayout descriptorSetLayout = VK_NULL_HANDLE;
	std::vector<VkDescriptorSet> descriptorSets;

public:
	// Defaulted constructor
	Cubemap() = default;

	// Destroy method for the resources
	void destroy();

	// [Re]Generate descriptor set layouts
	void generateDSL(const bool& reset = false);
};