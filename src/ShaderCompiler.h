#pragma once
#include <shaderc/shaderc.hpp>
#include <string>

/**
 * @brief Compiler for the shaders
 * Based on shaderc
*/
class ShaderCompiler
{
public:
	// Method for preprocessing shader
	std::string preprocessShader(const std::string& sourceName, shaderc_shader_kind kind, const std::string& source);

	// Method to compile string to spirv
	std::vector<uint32_t> compileToSPIRV(const std::string& sourceName, shaderc_shader_kind kind, const std::string& source, bool optimize = false);

	// Method to load a shader as a string
	std::string loadShader(const std::string& path);
};

