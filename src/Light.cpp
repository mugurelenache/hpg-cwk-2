#include "Light.h"
#include "VKC.h"
#include <iostream>

/**
 * @brief Destroy the descriptor set layout
*/
void Light::destroy()
{
	vkDestroyDescriptorSetLayout(VKC::get().device, descriptorSetLayout, nullptr);
}


/**
 * @brief [Re]generate the descriptor sets
 * @param reset
*/
void Light::generateDSL(const bool& reset)
{
	if (reset)
	{
		vkDestroyDescriptorSetLayout(VKC::get().device, descriptorSetLayout, nullptr);
	}

	// Create the bindings for the vertex shader
	std::vector<VkDescriptorSetLayoutBinding> bindings;
	{
		VkDescriptorSetLayoutBinding binding = {};
		binding.binding = 0;
		binding.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
		binding.descriptorCount = 1;
		binding.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
		binding.pImmutableSamplers = nullptr;
		bindings.push_back(binding);
	}

	// Create the descriptor set layout
	VkDescriptorSetLayoutCreateInfo dslCI = {};
	dslCI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
	dslCI.bindingCount = static_cast<uint32_t>(bindings.size());
	dslCI.pBindings = bindings.data();

	if (vkCreateDescriptorSetLayout(VKC::get().device, &dslCI, nullptr, &descriptorSetLayout) != VK_SUCCESS)
	{
		std::cerr << "Error: cannot create descriptor set layout" << std::endl;
		std::exit(-1);
	}

	// Make one layout for each descriptor set
	std::vector<VkDescriptorSetLayout> layouts(VKC::get().swapchainImages.size(), descriptorSetLayout);

	VkDescriptorSetAllocateInfo dsAI = {};
	dsAI.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
	dsAI.descriptorPool = VKC::get().descriptorPool;
	dsAI.descriptorSetCount = static_cast<uint32_t>(VKC::get().swapchainImages.size());
	dsAI.pSetLayouts = layouts.data();

	descriptorSets.resize(VKC::get().swapchainImages.size());

	// Allocate the descriptor sets
	if (vkAllocateDescriptorSets(VKC::get().device, &dsAI, descriptorSets.data()) != VK_SUCCESS)
	{
		std::cerr << "Error: failed to allocate descriptor sets." << std::endl;
		std::exit(-1);
	}

	// Update the set via the writes
	for (size_t i = 0; i < descriptorSets.size(); i++)
	{
		std::vector<VkWriteDescriptorSet> descriptorSetWrites;
		
		//  TRANSFORM
		VkDescriptorBufferInfo descriptorBI = {};
		descriptorBI.buffer = VKC::get().uniformBuffers[i].buffer;
		descriptorBI.offset = transform.offsetIntoBuffer;
		descriptorBI.range = sizeof(Transform);

		{
			VkWriteDescriptorSet descriptorWrite = {};
			descriptorWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
			descriptorWrite.dstSet = descriptorSets[i];
			descriptorWrite.dstBinding = 0;
			descriptorWrite.dstArrayElement = 0;
			descriptorWrite.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
			descriptorWrite.descriptorCount = 1;
			descriptorWrite.pBufferInfo = &descriptorBI;
			descriptorWrite.pImageInfo = nullptr;
			descriptorWrite.pTexelBufferView = nullptr;
			descriptorSetWrites.push_back(descriptorWrite);
		}

		vkUpdateDescriptorSets(VKC::get().device, static_cast<uint32_t>(descriptorSetWrites.size()), descriptorSetWrites.data(), 0, nullptr);
	}
}
