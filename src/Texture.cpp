#include "Texture.h"
#include <iostream>
#include <stb_image.h>
#include "Utility.h"
#include "VKC.h"
#include "Buffer.h"
#include <cmath>

/**
 * @brief Loads a texture from a file
 * @param path
*/
void Texture::load(const std::string& path, const bool& persistentData, const VkFormat& format)
{
	// Load the data via stbi
	pixels = stbi_load(path.c_str(), &width, &height, &channels, STBI_rgb_alpha);

	if (!pixels)
	{
		std::cerr << "Error: Could not load texture." << std::endl;
		std::exit(-1);
	}

	VkDeviceSize size = static_cast<VkDeviceSize>(width * height * 4);
	textureImageMipLevels = static_cast<uint32_t>(std::floor(std::log2(std::max(width, height)))) + 1;

	// Create a  staging buffer to transfer the data to the gpu
	Buffer stagingBuffer;
	stagingBuffer.create(size, VK_BUFFER_USAGE_TRANSFER_SRC_BIT, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT);

	stagingBuffer.bind();
	stagingBuffer.map(size);
	stagingBuffer.populateWith(pixels, size);
	stagingBuffer.unmap();

	if(!persistentData)
	{
		stbi_image_free(pixels);
	}

	// Creating the image
	Utility::createImage(VKC::get().physicalDevice, VKC::get().device, textureImage, textureImageMemory, width, height, 1, textureImageMipLevels, 1, VK_SAMPLE_COUNT_1_BIT, format, VK_IMAGE_TILING_OPTIMAL, VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT);

	// Transitioning it to transfer dst
	Utility::transitionImageLayout(VKC::get().device, VKC::get().commandPool, VKC::get().graphicsQueue, textureImage, format, VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, textureImageMipLevels);

	// Copy the data from buffer to image
	Utility::copyBufferToImage(VKC::get().device, VKC::get().commandPool, VKC::get().graphicsQueue, stagingBuffer.buffer, textureImage, width, height);

	// Destory the buffer as it's not needed
	stagingBuffer.destroy();

	// Generates the mip maps for the texture
	Utility::generateMipMaps(VKC::get().physicalDevice, VKC::get().device, VKC::get().commandPool, VKC::get().graphicsQueue, textureImage, format, static_cast<uint32_t>(width), static_cast<uint32_t>(height), textureImageMipLevels);

	// Creates the image view
	Utility::createImageView(VKC::get().device, textureImageView, textureImage, VK_IMAGE_VIEW_TYPE_2D, format, VK_IMAGE_ASPECT_COLOR_BIT, textureImageMipLevels, 1);

	// Creates a sampler
	VkSamplerCreateInfo samplerCI = {};
	samplerCI.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
	samplerCI.minFilter = VK_FILTER_LINEAR;
	samplerCI.magFilter = VK_FILTER_LINEAR;
	samplerCI.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerCI.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerCI.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
	samplerCI.anisotropyEnable = VK_TRUE;

	// Checks the max anisotropy
	VkPhysicalDeviceProperties properties = {};
	vkGetPhysicalDeviceProperties(VKC::get().physicalDevice, &properties);

	samplerCI.maxAnisotropy = properties.limits.maxSamplerAnisotropy;
	samplerCI.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
	samplerCI.unnormalizedCoordinates = VK_FALSE;
	samplerCI.compareEnable = VK_FALSE;
	samplerCI.compareOp = VK_COMPARE_OP_ALWAYS;
	// Mip mapping until maximum
	samplerCI.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
	samplerCI.mipLodBias = 0.0f;
	samplerCI.minLod = 0.0f;
	samplerCI.maxLod = static_cast<float>(textureImageMipLevels);

	// Try to create the sampler
	if (vkCreateSampler(VKC::get().device, &samplerCI, nullptr, &textureImageSampler) != VK_SUCCESS)
	{
		std::cerr << "Error: Could not create texture sampler." << std::endl;
		std::exit(-1);
	}
}

std::vector<unsigned char> Texture::getPixel(const uint32_t& x, const uint32_t& y)
{	
	unsigned bytePerPixel = 4;
	unsigned char* pixelOffset = pixels + (x + width * y) * 4;

	std::vector<unsigned char> chns =
	{
		pixelOffset[0],
		pixelOffset[1],
		pixelOffset[2],
		pixelOffset[3]
	};

	return chns;
}

unsigned char Texture::pixelToGrayscale(const std::vector<unsigned char>& pixel)
{
	uint32_t p0 = pixel[0];
	uint32_t p1 = pixel[1];
	uint32_t p2 = pixel[2];

	return (p0 + p1 + p2) / 3;
}


/**
 * @brief Destroys the resources associated to the texture
*/
void Texture::destroy()
{
	vkDestroySampler(VKC::get().device, textureImageSampler, nullptr);
	vkDestroyImageView(VKC::get().device, textureImageView, nullptr);
	vkDestroyImage(VKC::get().device, textureImage, nullptr);
	vkFreeMemory(VKC::get().device, textureImageMemory, nullptr);
}
