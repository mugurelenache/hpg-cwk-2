#include "ShaderCompiler.h"
#include <iostream>
#include <fstream>

/**
 * @brief Pre-process the shader with different setting
 * @param sourceName
 * @param kind
 * @param source
 * @return
*/
std::string ShaderCompiler::preprocessShader(const std::string& sourceName, shaderc_shader_kind kind, const std::string& source)
{
    shaderc::Compiler compiler;
    shaderc::CompileOptions options;
    shaderc::PreprocessedSourceCompilationResult result =
        compiler.PreprocessGlsl(source, kind, sourceName.c_str(), options);

    if (result.GetCompilationStatus() != shaderc_compilation_status_success)
    {
        std::cerr << "Error: Cannot compile shader " << sourceName << std::endl;
        std::exit(-1);
    }

    return { result.cbegin(), result.cend() };
}


/**
 * @brief Compiles the processed shader to SPIRV as well as optimizes it
 * @param sourceName
 * @param kind
 * @param source
 * @param optimize
 * @return
*/
std::vector<uint32_t> ShaderCompiler::compileToSPIRV(const std::string& sourceName, shaderc_shader_kind kind, const std::string& source, bool optimize)
{
    shaderc::Compiler compiler;
    shaderc::CompileOptions options;

    if (optimize)
    {
        options.SetOptimizationLevel(shaderc_optimization_level_size);
    }

    shaderc::CompilationResult module =
        compiler.CompileGlslToSpv(source, kind, sourceName.c_str(), options);

    if (module.GetCompilationStatus() != shaderc_compilation_status_success)
    {
        std::cerr << "Error: Cannot compile GLSL shader \""<< sourceName << "\" to SPIR-V." << std::endl;
        std::cerr << "Details: " << module.GetErrorMessage() << std::endl;
        std::exit(-1);
    }

    return { module.cbegin(), module.cend() };
}


/**
 * @brief Loads a shader from file
 * @param path
 * @return
*/
std::string ShaderCompiler::loadShader(const std::string& path)
{
    std::ifstream file(path, std::ios::ate | std::ios::binary);

    if (!file.is_open())
    {
        std::cerr << "Error: cannot open file " << path << std::endl;
        std::exit(-1);
    }

    size_t fileSize = static_cast<size_t>(file.tellg());

    std::vector<char> buffer(fileSize);

    file.seekg(0);
    file.read(buffer.data(), fileSize);
    file.close();

    return { buffer.cbegin(), buffer.cend() };
}